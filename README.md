# vimwiki-graph

Visualize where vimwiki pages link to.

## Installation (Linux)

Copy the `graph.sh` bash script to your vimwiki project.

## Usage

Perform the following steps in your terminal.

1. Navigate to your vimwiki project.
2. Run `./graph.sh`.
3. Paste the output in your preferred DOT viewer.

[:hint:] Without installation, you can view DOT files at https://edotor.net/. See also [graphviz](https://graphviz.org/).
